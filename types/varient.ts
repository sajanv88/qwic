export type Varient = {
    type: "default" | "serialNumber" | "invoiceNumber";
    min?: number;
    max?: number;
};
