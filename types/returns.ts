import { Payload } from "./payload";

export interface ReturnCase extends Payload {
    id: string;
    requestedAt: string;
}
