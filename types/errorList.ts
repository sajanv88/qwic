export interface ErrorListDetail {
    serial: string;
    invoice: string;
    otherReason: string;
}
