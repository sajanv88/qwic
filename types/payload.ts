export interface Payload {
    serial: string;
    invoice: string;
    reason: string;
    otherReason: string;
    comment?: string;
}
