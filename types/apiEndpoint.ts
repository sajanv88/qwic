export enum API_ENDPOINT {
    FETCH_ALL_CASES = "/api/all_returns",
    REGISTER_NEW_CASE = "/api/new_return_request",
}
