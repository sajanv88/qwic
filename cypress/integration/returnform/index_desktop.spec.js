describe("Return request form", () => {
    beforeEach(() => {
        cy.viewport(1280, 1024);
    });

    describe("When you vist request return form", () => {
        it("should land request form", () => {
            cy.visit("/");
        });

        it("should have form title", () => {
            cy.get('[data-cy="form-title"]').contains("Return request");
        });

        it("should have product title", () => {
            cy.get('[data-cy="product-title"]').contains(
                "Qwic perf rd10 dt diamond dutch orange"
            );
        });

        it("should have product code", () => {
            cy.get('[data-cy="product-code"]').contains("Hp00760");
        });

        it("should have order number", () => {
            cy.get('[data-cy="order-number"]').contains("#11304");
        });

        it("should have order description", () => {
            cy.get('[data-cy="order-description"]').contains(
                "Travel cable adapter"
            );
        });

        it("should have form heading", () => {
            cy.get('[data-cy="request-form-heading"]').contains(
                "Please provide us some details"
            );
        });

        it("should show manadatory text", () => {
            cy.get('[data-cy="mandatory"]').contains("Mandatory");
        });

        it("should validate serial number input", () => {
            cy.get('[data-cy="serial-input"]').type("test");
            cy.get('[data-cy="input-error"]').contains(
                "Serial number at least 6 character count and, not more than 10"
            );
        });

        it("should select reason from dropdown and, clear when clicked cross icon", () => {
            cy.get('[data-cy="dropdown"]').click();
            cy.get('[data-cy="dropdown"]').within(() => {
                cy.get('[data-cy="dropdown-option-0"]').click();
                cy.get('[data-cy="dropdown-label"]').contains("Broken part");
                cy.get('[data-cy="dropdown-clear"]').click();
                cy.get('[data-cy="dropdown-label"]').contains(
                    "Select the reason of your return item"
                );
            });
        });
    });

    describe("When submit form", () => {
        it("should show success message when the form got submit without an error", () => {
            cy.visit("/");
            cy.get('[data-cy="serial-input"]').type("test123");
            cy.get('[data-cy="invoice-input"]').type("111111");
            cy.get('[data-cy="dropdown"]').click();
            cy.get('[data-cy="dropdown"]').within(() => {
                cy.get('[data-cy="dropdown-option-0"]').click();
                cy.get('[data-cy="dropdown-label"]').contains("Broken part");
            });
            cy.get('[data-cy="comments-input"]').type("Test commments");
            cy.get('[data-cy="submit-btn"]').click();
            cy.get('[data-cy="request-form-successfully-submitted"]', {
                timeout: 10000,
            }).contains("Return request successfully submitted.");
        });

        it("should show while submit the form when there no error", () => {
            cy.get('[data-cy="submit-btn"]').click();
            cy.get('[data-cy="request-form-form-field-error"]').contains(
                "Please correct the mandatory fields."
            );
        });
    });
});
