FROM node:14.15.1
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 8000
RUN npm run build
CMD [ "npm", "run", "start"]