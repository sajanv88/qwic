# Qwic backend service

---

Get all registered returns

##### Get Request

Endpoint url: `/api/all_returns`

Example:

```bash
 curl -X GET -H "content-type:application/json" "http://localhost:8000/api/all_returns"
```

##### Response

status code `200`

```bash
{
    "data": [
        {
            "id": "RuUFhkG27B8kUanyMNcbq",
            "serial": "abcdef890",
            "invoice": "123456",
            "reason": "other",
            "otherReason": "Testing...",
            "comment": "Testing api endpoint",
            "requestedAt": "Wed, 17 Mar 2021 10:22:03 GMT"
        }
    ]
}
```

Create new return request.

##### Post Request

Endpoint url: `/api/new_return_request`
Request payload: `{ serial: string, invoice: string, reason: string, otherReason: string, comment: string }`

Example:

```bash
 curl -d '{"serial": "xrwfge3","invoice": "356743","reason": "broken_part","otherReason": "",
"comment": ""}' -X POST -H "content-type:application/json" "http://localhost:8000/api/new_return_request"
```

##### Success Response

status code `201`

```bash
{}
```

##### Failure Response

status code `400`

```bash
{
    "err": {
        "name": "Return Request Exception",
        "message": "Some fields are not correct.",
        "details": [
            {
                "serial": "Serial number at least 6 character count and, not more than 10"
            },
            {
                "invoice": "Invoice number at least 1 character count and, no more than 6 ."
            },
            {
                "otherReason": "Other reason mube be less than 240 characters"
            }
        ]
    }
}

```
