import express, { NextFunction, Request, Response } from "express";
import RegisterReturn from "../service/registerReturns";
import ReturnRequestException from "../exceptions/returnRequestException";

export default () => {
    const router = express.Router();
    const returns = new RegisterReturn();

    /**
     * @GET
     * Returns all the registered return cases
     */
    router.get(
        "/all_returns",
        async (req: Request, res: Response, next: NextFunction) => {
            res.status(200).json({ data: returns.getAllCases() });
        }
    );

    /**
     * @POST
     * Create a new return request.
     */
    router.post(
        "/new_return_request",
        async (req: Request, res: Response, next: NextFunction) => {
            try {
                await returns.registerCase(req.body);
                res.status(201).json({});
            } catch (e) {
                res.status(400).json({
                    err: new ReturnRequestException(
                        "Some fields are not correct."
                    ).toJson(),
                });
            }
        }
    );

    return router;
};
