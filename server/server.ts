import express, { Request, Response } from "express";
import next from "next";
import { json } from "body-parser";
import path from "path";
import api from "./api/router";

(async function () {
    const port = 8000;
    const dev = process.env.NODE_ENV !== "production";
    const app = next({ dev });

    await app
        .prepare()
        .catch((e) => console.error(`Failed to start application:`, e));

    const server = express();
    server.use(json({ limit: 2048 }));
    server.use(express.static(path.resolve(__dirname, "../public")));

    // api context path
    server.use("/api", api());

    // basic page rendering. Renders component which is defined inside pages directory
    server.all("*", (req: Request, res: Response) => {
        return app.render(req, res, req.url);
    });

    server.listen(port, () =>
        console.log(`Application is running  on ${port}`)
    );
})();
