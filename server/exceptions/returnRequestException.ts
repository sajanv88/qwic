class ReturnRequestException extends Error {
    constructor(message: string) {
        super();
        this.name = "Return Request Exception";
        this.message = message;
    }
    public toJson = () => {
        const obj = {
            name: this.name,
            message: this.message,
            details: [
                {
                    serial:
                        "Serial number at least 6 character count and, not more than 10",
                },
                {
                    invoice:
                        "Invoice number at least 1 character count and, no more than 6 .",
                },
                {
                    otherReason:
                        "Other reason mube be less than 240 characters",
                },
            ],
        };

        return obj;
    };
}

export default ReturnRequestException;
