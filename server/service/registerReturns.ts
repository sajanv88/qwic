import { nanoid } from "nanoid";
import { Payload } from "../../types/payload";
import { ReturnCase } from "../../types/returns";
import validateBeforeSubmit from "../../utils/validateBeforeSubmit";

// mock database layer
class RegisterReturn {
    private _allCases: ReturnCase[] = [];
    constructor() {
        this._allCases.push({
            id: nanoid(),
            serial: "abcdef890",
            invoice: "123456",
            reason: "other",
            otherReason: "Testing...",
            comment: "Testing api endpoint",
            requestedAt: new Date().toUTCString(),
        });
    }

    public getAllCases(): ReturnCase[] {
        return this._allCases;
    }

    public registerCase(payload: Payload): Promise<boolean | Error> {
        return new Promise(async (resolve, reject) => {
            const isFormFeildsAreNotValid = await validateBeforeSubmit(payload);
            if (isFormFeildsAreNotValid)
                return reject(new Error("Fields are not correct."));

            const newCase: ReturnCase = {
                ...payload,
                id: nanoid(),
                requestedAt: new Date().toUTCString(),
            };
            this._allCases.push(newCase);
            resolve(true);
        });
    }
}

export default RegisterReturn;
