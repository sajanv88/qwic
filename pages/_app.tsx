import { ReactElement } from "react";
import "../styles/globals.css";
interface MainAppProps {
    Component: React.ElementType;
    pageProps: React.ReactPropTypes;
}

function MainApp({ Component, pageProps }: MainAppProps): ReactElement {
    return <Component {...pageProps} />;
}

export default MainApp;
