import { ReactElement } from "react";
import OrderNumber from "../components/OrderNumber";
import ReturnForm from "../components/ReturnForm";

function IndexPage(): ReactElement {
    return (
        <div className="px-5 md:container md:mx-auto">
            <div className="w-full lg:w-1/2 lg:mx-auto">
                <div className="flex flex-col py-10">
                    <h1
                        className="text-2xl md:text-3xl xl:text-4xl mb-3 font-bold"
                        data-cy="form-title"
                    >
                        Return request
                    </h1>
                    <h2
                        className="uppercase text-lg md:text-xl xl:text-2xl text-gray-700 mb-1"
                        data-cy="product-title"
                    >
                        Qwic perf rd10 dt diamond dutch orange
                    </h2>
                    <span
                        className="text-gray-600 uppercase"
                        data-cy="product-code"
                    >
                        Hp00760
                    </span>

                    <OrderNumber
                        imageUrl="/travel-adapter-cable.png"
                        displayName="Travel adapter"
                        orderNumber="11304"
                        description="Travel cable adapter"
                    />

                    <ReturnForm />
                </div>
            </div>
        </div>
    );
}

export default IndexPage;
