import React, { ReactElement, useEffect, useState, memo } from "react";
import classNames from "classnames";
import { FaStar } from "react-icons/fa";
import { Varient } from "../types/varient";

import validateInvoiceNumber from "../utils/validateInvoiceNumber";
import validateSerialNumber from "../utils/validateSerialNumber";

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
    varient: Varient;
    onInputFinished: (value: string) => void;
    onInputError?: (error: string) => void;
    errorMsg?: string;
    withRequired?: boolean;
}

function Input({
    varient,
    onInputFinished,
    onInputError,
    errorMsg,
    withRequired,
    ...props
}: InputProps): ReactElement {
    const [error, setError] = useState<string>("");

    useEffect(() => {
        if (errorMsg) {
            setError(errorMsg);
        }
    }, [errorMsg]);

    useEffect(() => {
        if (error && onInputError) onInputError(error);
    }, [error]);

    if (
        (varient.type === "invoiceNumber" || varient.type === "serialNumber") &&
        (!varient.min || !varient.max)
    ) {
        throw new Error(
            "Minimum and Maximum value must be initalised when using invoiceNumber or serialNumber varient"
        );
    }

    const onChangeHandler = async (
        e: React.SyntheticEvent<HTMLInputElement>
    ): Promise<void> => {
        const { value } = e.target as HTMLInputElement;
        if (varient.type === "invoiceNumber") {
            const { val, error } = await validateInvoiceNumber(varient, value);
            onInputFinished(val);
            setError(error);
        } else if (varient.type === "serialNumber") {
            const { val, error } = await validateSerialNumber(varient, value);
            onInputFinished(val);
            setError(error);
        } else {
            if (varient.max! && value.length >= varient.max) {
                setError(`Maximum character count is ${varient.max}`);
            } else {
                onInputFinished(value);
                setError("");
            }
        }
    };

    return (
        <>
            {withRequired && (
                <div
                    className={classNames("text-red-700 text-sm flex", {
                        hidden: props.value,
                    })}
                >
                    <span className="mr-1" data-cy="mandatory">
                        Mandatory
                    </span>
                    <FaStar />
                </div>
            )}
            <input
                {...props}
                onChange={onChangeHandler}
                value={props.value}
                className={classNames(
                    "block py-5 px-4 w-full border border-gray-500 rounded text-lg text-gray-600 ".concat(
                        props.className!
                    ),
                    {
                        "border-red-700": error,
                    }
                )}
            />
            {error && (
                <span
                    className="text-sm text-red-700 block mt-2 ml-4"
                    data-cy="input-error"
                >
                    {error}
                </span>
            )}
        </>
    );
}

export default memo(Input);
