import { ReactElement, memo, useState } from "react";
import classNames from "classnames";
import { FaStar } from "react-icons/fa";

import isMaximumCharacter from "../utils/isMaximumCharacter";

interface TextareaProps
    extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {
    onCommentEvent: (val: string) => void;
    errorMsg?: string;
    withRequired?: boolean;
}
function Textarea({
    onCommentEvent,
    errorMsg,
    withRequired,
    ...props
}: TextareaProps): ReactElement {
    const [error, setError] = useState<boolean>(false);

    const onChangeHandler = (e: React.SyntheticEvent<HTMLTextAreaElement>) => {
        const { value } = e.target as HTMLTextAreaElement;
        if (props.maxLength) {
            setError(isMaximumCharacter(value, props.maxLength));
        }
        onCommentEvent(value);
    };

    const val = props.value as string;
    return (
        <>
            {withRequired && (
                <div
                    className={classNames(
                        "text-red-700 text-sm flex items-center",
                        {
                            hidden: val,
                        }
                    )}
                >
                    <span className="mr-1">Mandatory</span>
                    <FaStar />
                </div>
            )}
            <textarea
                {...props}
                value={val}
                className={classNames(
                    "resize-none w-full h-40 border border-gray-500 rounded py-5 px-4 text-lg text-gray-600",
                    {
                        "border-red-700": error,
                    }
                )}
                onChange={onChangeHandler}
            />

            <div className="flex justify-between items-center">
                <span
                    className={classNames("text-sm block mt-2 ml-4", {
                        "text-red-700 visible": errorMsg,
                        invisible: !errorMsg,
                    })}
                >
                    {errorMsg}
                </span>

                {props.maxLength && (
                    <span
                        className={classNames("text-sm block mt-2 ml-4", {
                            "text-red-700": error,
                        })}
                    >
                        {val.length}/{props.maxLength!}
                    </span>
                )}
            </div>
        </>
    );
}

export default memo(Textarea);
