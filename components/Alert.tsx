import React, { ReactElement } from "react";
import classNames from "classnames";
import { FaInfoCircle, FaTimesCircle } from "react-icons/fa";
interface AlertProps extends React.HTMLAttributes<HTMLDivElement> {
    kind: "info" | "success" | "danger";
    children: React.ReactNode;
    onDismiss?: () => void;
}
function Alert({
    kind,
    onDismiss,
    children,
    ...props
}: AlertProps): ReactElement {
    return (
        <div
            {...props}
            className={classNames("px-3 py-4 text-white mb-3", {
                "bg-red-400": kind === "danger",
                "bg-blue-400": kind === "info",
                "bg-green-400": kind === "success",
            })}
        >
            <div className="flex items-center">
                <FaInfoCircle className="mr-3 text-4xl" />
                <div className="flex-1">{children}</div>
                {onDismiss && (
                    <FaTimesCircle
                        className="text-2xl"
                        onClick={() => onDismiss()}
                    />
                )}
            </div>
        </div>
    );
}

export default Alert;
