import { ReactElement, memo } from "react";
import classNames from "classnames";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    children: React.ReactNode;
    kind?: "primary" | "secondary" | "default";
}
function Button({
    children,
    kind = "default",
    ...props
}: ButtonProps): ReactElement {
    return (
        <button
            {...props}
            className={classNames(
                "py-5 text-white w-full border border-gray-500 ".concat(
                    props.className!
                ),
                {
                    "bg-orange-500": kind === "primary",
                    "bg-black": kind === "secondary",
                    "text-gray-800": kind === "default",
                }
            )}
        >
            <div className="flex justify-start pl-5 md:pl-10">{children}</div>
        </button>
    );
}

export default memo(Button);
