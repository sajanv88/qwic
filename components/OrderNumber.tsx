import { ReactElement } from "react";

interface OrderNumberProps {
    imageUrl: string;
    orderNumber: string;
    description: string;
    displayName: string;
}
function OrderNumber({
    imageUrl,
    orderNumber,
    description,
    displayName,
}: OrderNumberProps): ReactElement {
    return (
        <div className="flex mt-5 border-b-2 pb-10 mt-10">
            <div className="flex flex-col flex-1">
                <span
                    className="text font-bold text-gray-800"
                    data-cy="order-number"
                >
                    #{orderNumber}
                </span>
                <p
                    className="text md:text-lg lg:text-xl xl:text-2xl text-gray-800"
                    data-cy="order-description"
                >
                    {description}
                </p>
            </div>
            <div className="w-20 h-12 bg-gray-300 flex justify-center p-1">
                <img src={imageUrl} alt={displayName} />
            </div>
        </div>
    );
}

export default OrderNumber;
