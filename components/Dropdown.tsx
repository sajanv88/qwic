import { ReactElement, useEffect, useRef, useState, memo } from "react";
import { FaChevronDown, FaTimes, FaStar } from "react-icons/fa";
import classNames from "classnames";
import useOutsideClick from "../hooks/outsideClick";
import { Option } from "../types/option";

interface DropdownProps {
    data: Option[];
    placeholder?: string;
    onSelectEvent: (selected: Option) => void;
    errorMsg?: string;
    clear: boolean;
    withRequired?: boolean;
}

function Dropdown({
    data,
    placeholder,
    onSelectEvent,
    errorMsg,
    withRequired,
    clear,
}: DropdownProps): ReactElement {
    const [toggleOption, setToggleOption] = useState<boolean>(false);
    const [selected, setSelected] = useState<Option>({ label: "", value: "" });
    const [error, setError] = useState<string>("");
    const [list, setList] = useState<Option[]>([]);
    const dropdownRef = useRef<HTMLDivElement | null>(null);
    useOutsideClick(dropdownRef, () => {
        setToggleOption(false);
    });

    useEffect(() => {
        setList(data);
    }, []);

    useEffect(() => {
        if (clear) {
            onClearHandler();
        }
    }, [clear]);

    useEffect(() => {
        if (errorMsg) setError(errorMsg);
    }, [errorMsg]);

    useEffect(() => {
        if (selected.value) {
            onSelectEvent(selected);
            setError("");
        }
    }, [selected]);

    const onSelectHandler = (option: Option) => {
        setList([...data.filter((item) => item.value !== option.value)]);
        setSelected(option);
    };

    const onClearHandler = () => {
        setSelected({ label: "", value: "" });
        setList([...data]);
        onSelectEvent({ label: "", value: "" });
    };

    const localPlaceholder = placeholder || "Select from the list";
    return (
        <>
            {withRequired && (
                <div
                    className={classNames("text-red-700 text-sm flex", {
                        hidden: selected.value,
                    })}
                >
                    <span className="mr-1">Mandatory</span> <FaStar />
                </div>
            )}
            <div
                className="relative block py-5 px-4 w-full border border-gray-500 rounded text-lg text-gray-600 bg-white"
                ref={dropdownRef}
                data-cy="dropdown"
                role="select"
                onClick={() => setToggleOption((t) => !t)}
            >
                <div className="flex items-center text-gray-500">
                    <span
                        className="flex-1 select-none"
                        data-cy="dropdown-label"
                    >
                        {!selected.label ? localPlaceholder : selected.label}
                    </span>
                    <span>
                        {!selected.label ? (
                            <FaChevronDown />
                        ) : (
                            <FaTimes
                                onClick={onClearHandler}
                                data-cy="dropdown-clear"
                            />
                        )}
                    </span>
                </div>
                <div
                    className={classNames(
                        "absolute w-full bg-white left-0 border border-gray-500 rounded",
                        {
                            hidden: !toggleOption,
                        }
                    )}
                    style={{
                        top: "4.2rem",
                    }}
                >
                    {list.map((option, idx) => (
                        <div
                            key={idx}
                            className="py-2 px-3 hover:bg-gray-300 select-none text-gray-500 hover:text-gray-700"
                            data-cy={`dropdown-option-${idx}`}
                            onClick={() => onSelectHandler(option)}
                        >
                            {option.label}
                        </div>
                    ))}
                </div>
            </div>
            {error && (
                <span className="text-sm text-red-700 block mt-2 ml-4">
                    {error}
                </span>
            )}
        </>
    );
}

export default memo(Dropdown);
