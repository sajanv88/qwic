import React, { ReactElement, useState } from "react";
import axios from "axios";
import { API_ENDPOINT } from "../types/apiEndpoint";
import { Option } from "../types/option";
import { Payload } from "../types/payload";
import Button from "./Button";
import Dropdown from "./Dropdown";
import Input from "./Input";
import Textarea from "./Textarea";
import Alert from "./Alert";
import validateBeforeSubmit from "../utils/validateBeforeSubmit";
import ErrorList from "./ErrorList";

const initalPayloadState = {
    serial: "",
    invoice: "",
    reason: "",
    otherReason: "",
    comment: "",
};

function ReturnForm(): ReactElement {
    const [payload, setPayload] = useState<Payload>(initalPayloadState);

    const [formFieldsError, setFormFieldsError] = useState<string>("");
    const [success, setSuccess] = useState<boolean>(false);
    const [errorDetails, setErrorDetails] = useState<any | null>(null);

    const onSerialNumberHandler = (serial: string) => {
        setPayload({ ...payload, serial });
    };

    const onInvoiceNumberHandler = (invoice: string) => {
        setPayload({ ...payload, invoice });
    };

    const onDropdownSelectEventHandler = (selected: Option) => {
        if (payload.reason === "other") {
            payload.otherReason = "";
        }
        setPayload({ ...payload, reason: selected.value });
    };

    const onCancelHandler = () => {
        setPayload(initalPayloadState);
    };

    const onSubmitHandler = async (
        e: React.SyntheticEvent<HTMLFormElement>
    ) => {
        e.preventDefault();
        setFormFieldsError("");
        setErrorDetails(null);

        const isFormFeildsAreNotValid = await validateBeforeSubmit(payload);
        if (isFormFeildsAreNotValid)
            return setFormFieldsError("Please correct the mandatory fields.");

        try {
            await axios.post(API_ENDPOINT.REGISTER_NEW_CASE, payload);
            setPayload({ ...initalPayloadState });
            setSuccess(true);
        } catch (e) {
            if (e.response.data && e.response.data.err) {
                const { err } = e.response.data;
                setErrorDetails(err);
            }
        }
    };

    return (
        <div className="flex flex-col">
            <h3
                className="mb-10 mt-10 text-lg text-gray-600"
                data-cy="request-form-heading"
            >
                Please provide us some details
                {formFieldsError && (
                    <span
                        className="text-sm text-red-700 block mt-2"
                        data-cy="request-form-form-field-error"
                    >
                        {formFieldsError}
                    </span>
                )}
            </h3>

            {success && (
                <Alert
                    kind="success"
                    onDismiss={() => setSuccess(false)}
                    data-cy="request-form-successfully-submitted"
                >
                    Return request successfully submitted.
                </Alert>
            )}
            {errorDetails && (
                <Alert kind="danger" onDismiss={() => setErrorDetails(null)}>
                    <ErrorList errorDetails={errorDetails.details} />
                </Alert>
            )}
            <form onSubmit={onSubmitHandler}>
                <div className="mb-10">
                    <Input
                        varient={{ type: "serialNumber", min: 6, max: 10 }}
                        onInputFinished={onSerialNumberHandler}
                        placeholder="Please provide us your part's serial number"
                        value={payload.serial}
                        data-cy="serial-input"
                        withRequired
                    />
                </div>
                <div className="mb-10">
                    <Dropdown
                        placeholder="Select the reason of your return item"
                        data={[
                            { label: "Broken part", value: "broken_part" },
                            { label: "Unused part", value: "unused_part" },
                            { label: "Other", value: "other" },
                        ]}
                        clear={!payload.reason}
                        withRequired
                        onSelectEvent={onDropdownSelectEventHandler}
                    />
                    {payload.reason === "other" && (
                        <Textarea
                            maxLength={240}
                            placeholder="What is your other reason? Please tell us more!"
                            value={payload.otherReason}
                            data-cy="other-reason-input"
                            withRequired
                            onCommentEvent={(otherReason) =>
                                setPayload({ ...payload, otherReason })
                            }
                        />
                    )}
                </div>
                <div className="mb-10">
                    <Input
                        varient={{ type: "invoiceNumber", min: 1, max: 6 }}
                        onInputFinished={onInvoiceNumberHandler}
                        placeholder="Provide an Invoice number"
                        value={payload.invoice}
                        data-cy="invoice-input"
                        withRequired
                    />
                </div>
                <div className="mb-10">
                    <Textarea
                        maxLength={240}
                        data-cy="comments-input"
                        placeholder="Comments"
                        value={payload.comment}
                        onCommentEvent={(comment) =>
                            setPayload({ ...payload, comment })
                        }
                    />
                </div>
                <div className="mb-10 flex">
                    <Button
                        className="uppercase mr-5"
                        type="button"
                        onClick={onCancelHandler}
                        data-cy="cancel-btn"
                    >
                        Cancel
                    </Button>
                    <Button
                        type="submit"
                        kind="secondary"
                        className="uppercase ml-5 hover:bg-orange-500"
                        data-cy="submit-btn"
                    >
                        Submit
                    </Button>
                </div>
            </form>
        </div>
    );
}

export default ReturnForm;
