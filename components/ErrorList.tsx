import React, { ReactElement } from "react";
import { nanoid } from "nanoid";
import { ErrorListDetail } from "../types/errorList";

interface ErrorListProps {
    errorDetails: ErrorListDetail[];
}
function ErrorList({ errorDetails }: ErrorListProps): ReactElement {
    return (
        <div className="flex flex-col">
            <p data-cy="error-form-failed">Failed to submit your request.</p>
            {errorDetails.map((detail: ErrorListDetail) => (
                <React.Fragment key={nanoid()}>
                    <span
                        className="block text-sm"
                        data-cy="serial-description"
                    >
                        {detail.serial}
                    </span>
                    <span
                        className="block text-sm"
                        data-cy="invoice-description"
                    >
                        {detail.invoice}
                    </span>
                    <span
                        className="block text-sm"
                        data-cy="other-reason-description"
                    >
                        {detail.otherReason}
                    </span>
                </React.Fragment>
            ))}
        </div>
    );
}

export default ErrorList;
