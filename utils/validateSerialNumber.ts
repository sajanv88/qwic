import { Varient } from "../types/varient";

const validateSerialNumber = async (
    varient: Varient,
    value: string
): Promise<{ error: string; val: string }> => {
    if (value.length < varient.min! || value.length > varient.max!) {
        return {
            val: value,
            error: `Serial number at least ${varient.min} character count and, not more than ${varient.max}`,
        };
    }
    return {
        val: value,
        error: "",
    };
};

export default validateSerialNumber;
