import { Varient } from "../types/varient";

const validateInvoiceNumber = async (
    varient: Varient,
    value: string
): Promise<{ error: string; val: string }> => {
    const val = value.replace(/[^0-9.]/g, "").replace(/(\..*?)\..*/g, "$1");
    if (val.length > varient.max!) {
        return {
            val,
            error: `Invoice number must not be more than ${varient.max} character count.`,
        };
    }
    return { val, error: "" };
};

export default validateInvoiceNumber;
