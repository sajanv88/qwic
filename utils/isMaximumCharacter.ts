const isMaximumCharacter = (value: string, max: number): boolean => {
    return value.length >= max;
};

export default isMaximumCharacter;
