import { Payload } from "../types/payload";
import isMaximumCharacter from "./isMaximumCharacter";
import validateInvoiceNumber from "./validateInvoiceNumber";
import validateSerialNumber from "./validateSerialNumber";

const validateBeforeSubmit = async ({
    serial,
    invoice,
    reason,
    otherReason,
}: Payload) => {
    if (
        !serial ||
        !reason ||
        (reason === "other" && !otherReason) ||
        !invoice
    ) {
        return true;
    }

    const serialNumber = await validateSerialNumber(
        {
            type: "serialNumber",
            min: 6,
            max: 10,
        },
        serial
    );

    if (serialNumber.error) return true;

    const invoiceNumber = await validateInvoiceNumber(
        { type: "invoiceNumber", min: 1, max: 6 },
        invoice
    );

    if (invoiceNumber.error) return true;

    if (reason === "other" && isMaximumCharacter(otherReason, 240)) return true;

    return false;
};

export default validateBeforeSubmit;
